const mongoose = require('mongoose')
const db = require('../db')
const Book = require('../models/Book')

const books = [
  {
    Title: 'El Quijote',
    Author: 'Miguel de Cervantes',
    Publisher: 'Alfaguara',
    Year: 2015,
    ISBN: '8420412147',
    Language: 'Spanish',
    Genre: 'Novel',
  },
  {
    Title: 'Maus',
    Author: 'Art Spiegelman',
    Publisher: 'Penguin',
    Year: 2003,
    ISBN: '9780141014081',
    Language: 'English',
    Genre: 'Graphic novel',
  },
  {
    Title: 'No pienses en un elefante',
    Author: 'George Lakoff',
    Publisher: 'Atalaya',
    Year: 2017,
    ISBN: '849942595X',
    Language: 'Spanish',
    Genre: 'Essay',
  },
  {
    Title: '1984',
    Author: 'George Orwell',
    Publisher: 'DEBOLSILLO',
    Year: 2013,
    ISBN: ' 8499890946',
    Language: 'Spanish',
    Genre: 'Novel',
  },
]

mongoose
  .connect(db.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(async () => {
    const allBooks = await Book.find()

    if (allBooks.length) {
      console.log('Deleting all books')
      await Book.collection.drop()
    }
  })
  .catch((error) => {
    console.log('Error deleting data:', error)
  })
  .then(async () => {
    await Book.insertMany(books)
    console.log('Data inserted correctly')
  })
  .catch((error) => {
    console.log('Error creating data:', error)
  })
  .finally(() => mongoose.disconnect())
