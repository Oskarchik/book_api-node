const express = require('express')
const Book = require('../models/Book')

const router = express.Router()

router.get('/', async (req, res, next) => {
  try {
    const books = await Book.find()
    return res.json(books)
  } catch (error) {
    console.log(error)
    next(new Error(error))
  }
})

router.get('/:id', async (req, res, next) => {
  try {
    const { id } = req.params
    const book = await Book.findById(id)
    if (book) {
      return res.status(200).json(book)
    } else {
      res.status(404).json('Book not found')
    }
  } catch (error) {
    next(new Error(error))
  }
})
router.delete('/:id', async (req, res, next) => {
  try {
    const { id } = req.params
    const deleted = await Book.findByIdAndDelete(id)
    if (deleted) {
      return res.status(200).json('Book deleted')
    }
    return res.status(200).json('Book not found')
  } catch (error) {
    next(new Error(error))
  }
})

router.put('/:id', async (req, res, next) => {
  try {
    const { id } = req.params
    const update = req.body

    const updatedBook = await Book.findByIdAndUpdate(id, update, { new: true })

    if (update) {
      res.status(200).json('Book updated')
    }
  } catch (error) {
    next(new Error(error))
  }
})

router.post('/create', async (req, res, next) => {
  try {
    const { Title, Author, Publisher, Year, ISBN, Language, Genre } = req.body
    const newBook = new Book({
      Title,
      Author,
      Publisher,
      Year,
      ISBN,
      Language,
      Genre,
    })

    const createdBook = await newBook.save()

    return res.status(200).json(createdBook)
  } catch (error) {
    next(new Error(error))
  }
})

router.get('/genre/:genre', async (req, res, next) => {
  try {
    const { genre } = req.params
    const booksByGenre = await Book.find({ Genre: genre })

    if (booksByGenre) {
      return res.status(200).json(booksByGenre)
    } else {
      return res.status(404).json('Genre not found')
    }
  } catch (error) {
    next(new Error(error))
  }
})

router.get('/publishers/:publisher', async (req, res, next) => {
  try {
    const { publisher } = req.params
    const booksByPublisher = await Book.find({ publisher: publisher })

    if (booksByPublisher) {
      res.status(200).json(booksByPublisher)
    } else {
      res.status(404).json('Publisher not found')
    }
  } catch (error) {
    next(new Error(error))
  }
})

module.exports = router
