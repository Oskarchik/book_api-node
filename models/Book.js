const mongoose = require('mongoose')

const Schema = mongoose.Schema

const bookSchema = new Schema(
  {
    creatorId: {type: mongoose.Types.ObjectId, ref: 'User'},
    Title: { type: String, required: true },
    Author: { type: String },
    Publisher: { type: String },
    Year: { type: Number },
    ISBN: { type: String, required: true },
    Language: { type: String },
    Genre: { type: String },
  },
  {
    timestamps: true,
  },
)

const Book = mongoose.model('Book', bookSchema)

module.exports = Book
