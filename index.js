const express = require('express')
const db = require('./db')

const bookRoutes = require('./routes/books.routes')

db.connect()

const PORT = 3001

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/books', bookRoutes)

app.use('*', (req, res, next) => {
  const err = new Error('Route not found')
  err.status = 404
  next(new Error(err))
})

app.use((err, req, res, next) => {
  return res.status(err.status || 500).json(err.message || 'Unexpected error')
})

app.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`)
})
